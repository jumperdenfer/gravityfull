package io.gitlab.jumperdenfer.gravityfull.runners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * Set up the runner and apply gravity on blocks
 * @author jumperdenfer
 *
 */
public class GravityRun implements Runnable {
	private Plugin gravity;
	private Block block;
	private FileConfiguration config;
	private List<Material> blockList = new ArrayList<Material>();
	private int maxrecursion;
	private int distance;
	
	
	/**
	 * 
	 * @param gravityFull
	 * @param block
	 * @param maxrecursion The max recursion applied on this class, default to 10, check the class "features.Gravity"
	 */
	public GravityRun(Plugin gravityFull,Block block, int maxrecursion) {
		this.gravity = gravityFull;
		this.block = block;
		this.blockList.add(Material.AIR);
		this.config = gravityFull.getConfig();
		this.addBlockInList();
		this.maxrecursion = maxrecursion;
		this.distance = this.config.getInt("maxdistance");
	}
	
	@Override
	public void run() {
		this.maxrecursion--;
		if(maxrecursion <= 0) {
			return;
		}
		Block underblock = block.getRelative(BlockFace.DOWN);
		Material underType = underblock.getType();
		List<Block> surroundingBlockForCheck = this.getSurroundingBlock(block,this.distance);
		List<Block> surroundingBlock = this.getSurroundingBlock(block);
		if(block.getType().isBlock() 
			&& this.checkIsValideBlock(block)
			&& this.canFall(block, surroundingBlockForCheck,this.distance)
			&& (
					underType == Material.AIR
					|| underType == Material.CAVE_AIR
					|| underblock.isLiquid()
			)
		){
			block.getWorld().spawnFallingBlock(block.getLocation().add(0.5D, 0.0D, 0.5D), block.getBlockData());
			block.setType(Material.AIR);
			block.breakNaturally();
		}
		
        for(Block nextBlock : surroundingBlock) {
        	if(nextBlock.getType() != Material.AIR && blockList.contains(nextBlock.getType())) {
        		gravity.getServer().getScheduler().runTaskLater(gravity, new GravityRun(gravity,nextBlock,this.maxrecursion),5);
        	}
        }
        
	}
	
	private void addBlockInList() {
		List<String> listFromConfig = this.config.getStringList("blocktogravity");
		for(String materialName : listFromConfig) {
			Material material = Material.getMaterial(materialName);
			blockList.add(material);
		}
	}
	
	/**
	 * Get surrouding block for break or place event
	 * @param block
	 * @return
	 */
	private List<Block> getSurroundingBlock(Block block) {
		List<Block> gravityBlock = new ArrayList<Block>();
		gravityBlock.add(block.getRelative(BlockFace.UP));
		gravityBlock.add(block.getRelative(BlockFace.DOWN));
		gravityBlock.add(block.getRelative(BlockFace.EAST));
		gravityBlock.add(block.getRelative(BlockFace.WEST));
		gravityBlock.add(block.getRelative(BlockFace.NORTH));
		gravityBlock.add(block.getRelative(BlockFace.SOUTH));
		return gravityBlock;
	}
	/**
	 * Get surrounding block in area for break or place event
	 * @param block
	 * @param distance
	 * @return
	 */
	private List<Block> getSurroundingBlock(Block block,int distance) {
		List<Block> gravityBlock = new ArrayList<Block>();
		for(int i = 0; i <= distance; i++) {
			gravityBlock.add(block.getRelative(BlockFace.UP,i));
			gravityBlock.add(block.getRelative(BlockFace.DOWN,i));
			gravityBlock.add(block.getRelative(BlockFace.EAST,i));
			gravityBlock.add(block.getRelative(BlockFace.WEST,i));
			gravityBlock.add(block.getRelative(BlockFace.NORTH,i));
			gravityBlock.add(block.getRelative(BlockFace.SOUTH,i));
		}
		return gravityBlock;
	}
	
	/**
	 * Check if block can be modified by gravity
	 * @param block
	 * @return
	 */
	private boolean checkIsValideBlock(Block block) {
		if( this.blockList.contains(block.getType())){
				return true;
			}
		return false;
	}
	
	/**
	 * Check if block have at least one solid block for not falling
	 * @param block
	 * @param surroundingBlock
	 * @return
	 */
	private boolean canFall(Block block,List<Block>surroundingBlock,int recursion) {
		recursion--;
		boolean canFall = true;
		if(block.getType() == Material.AIR) {
			return false;
		}
		for(Block nextBlock : surroundingBlock) {
			if(recursion > 0 && this.checkIsValideBlock(nextBlock) && nextBlock.getType() != Material.AIR ) {
				List<Block> nearBlock = this.getSurroundingBlock(nextBlock);
				boolean valideFall = this.canFall(nextBlock, nearBlock, recursion);
				if(!valideFall) {
					canFall = false;
					break;
				}
			}
			else {
				boolean isSoft = this.checkIsValideBlock(nextBlock);
				if(!isSoft) {
					canFall = false;
					break;
				}
			}
			
		}
		
		return canFall;
	}
	
	
	
	
	
}
