package io.gitlab.jumperdenfer.gravityfull;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import io.gitlab.jumperdenfer.gravityfull.features.Gravity;

public class GravityFull extends JavaPlugin {

	@Override
	public void onEnable() {
		
		File configFile = new File(getDataFolder(),"config.yml");
		if(!configFile.exists()) {
			this.saveDefaultConfig();
			this.saveResource("config.yml", false);
		}
		this.getServer().getPluginManager().registerEvents(new Gravity(this),this);
		getLogger().info("Gravity Full enabled");
	}
	
	@Override
	public void onDisable() {
		getLogger().info("Gravity Full disabled");
	}
}
