package io.gitlab.jumperdenfer.gravityfull.features;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;

import io.gitlab.jumperdenfer.gravityfull.runners.GravityRun;

/**
 * Handle all event about gravity
 * @author jumperdenfer
 *
 */
public class Gravity implements Listener {

	private Plugin gravityFull;
	
	public Gravity(Plugin gravityFull) {
		this.gravityFull = gravityFull;
	}
	

	
	
	@EventHandler()
	public void onBreakBlock(BlockBreakEvent blockBreakEvent) {
		Block block = blockBreakEvent.getBlock();
		gravityFull.getServer().getScheduler().runTaskLater(gravityFull, new GravityRun(gravityFull,block,5),5);
	}
	
	@EventHandler()
	public void onPlaceBlock(BlockPlaceEvent blockPlaceEvent) {
		Block block = blockPlaceEvent.getBlock();
		gravityFull.getServer().getScheduler().runTaskLater(gravityFull, new GravityRun(gravityFull,block,5),5);
	}
	
}
